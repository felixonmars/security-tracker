A DSA is needed for the following source packages in old/stable. The specific
CVE IDs do not need to be listed, they can be gathered in an up-to-date manner from
https://security-tracker.debian.org/tracker/source-package/SOURCEPACKAGE
when working on an update.

Some packages are not tracked here:
- Linux kernel (tracking in kernel-sec repo)
- Embargoed issues continue to be tracked in separate file.

To pick an issue, simply add your uid behind it.

If needed, specify the release by adding a slash after the name of the source package.

--
chromium
--
curl (ghedo)
--
knot-resolver
  Santiago Ruano Rincón proposed a debdiff for review
--
lilypond (jmm)
--
linux (carnil)
  Wait until more issues have piled up
--
net-snmp (carnil)
  Maintainer is proposing an update
--
nginx
--
rails (jmm)
  Sylvain Beucler proposed to help for the update, remaining CVEs to be done
--
ruby-kramdown
--
teeworlds (jmm)
--
xcftools
  Hugo proposed to work on this update
--
